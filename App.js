import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Component6 from './components/sample';
import ToDoContainer from './components/to-do-container';
export default class App extends React.Component {
  render() {
    return (
      <ToDoContainer/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
