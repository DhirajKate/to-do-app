
import React from 'react';
import {
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';

import styles from './style';

const EmptyTaskList = (props) => (
    <View>
        <Image source={require("../assets/No_More_Tasks_Empty_State_V1.png")} style={{ width: '100%', height: 200 }} />
        <Text style={{ alignSelf: "center", color: '#b3b3b3', marginTop: 5, fontSize: 16 }}>Your task list is empty!</Text>
    </View>
)
export default EmptyTaskList;

