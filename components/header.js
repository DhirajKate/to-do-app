
import React from 'react';
import {
    Text,
    View,
    Image
  } from 'react-native';

import styles from './style';

const HeaderComponent = ()=>(
    <View style={styles.header}>
    <Image style={{ width: 40, height: 40 }}
      source={require('../assets/Logo_Inside_150x150.png')} />
    <Text style={styles.logoText}>My Tasks</Text>
  </View>
);

export default HeaderComponent;