/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Platform,
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import SwitchSelector from 'react-native-switch-selector';

const options = [
  { label: 'To Do', value: '1' },
  { label: 'Completed', value: '2' }
];

export default class Component6 extends Component {
  constructor(){
    super();
    this.state={
      selectedTab:1
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={{ width: 40, height: 40 }}
            source={require('../assets/Logo_Inside_150x150.png')} />
          <Text style={styles.logoText}>My Tasks</Text>
        </View>
        <View style={styles.switchButton}>
          <SwitchSelector 
            options={options} 
            initial={0} 
            height={50}
            buttonColor='#30C6DE'
            fontSize={15}
            textColor='#8B8B8B'
            onPress={value => this.setState({selectedTab:value})} />
        </View>
        <View style={styles.searchBoxContainer}>
          <TextInput
            style={styles.inputText}
            underlineColorAndroid='transparent'
            placeholder="Add to My Tasks"
          />
          <View style={styles.roundButtonContainer}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={styles.roundButton}>
                <Image source={require("../assets/add.png")} tintColor={ "#707070"} style={{width:12, height:12}}/>
                {/* <Text style={styles.roundButtonText}>+</Text> */}
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView style={styles.taskList}>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>1</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Lorem ipsum dolor sit amet
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>2</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Morbi ac ex lobortis
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>3</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Mauris nec diam lacinia
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>4</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Cras vitae arcu ac metus
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>5</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Integer ut diam mattis
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>6</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Ut sed purus et leo
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>7</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Morbi vestibulum lectus eget
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>8</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Aliquam porta mi et ex
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>9</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Maecenas sit amet justo cursus
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.listItem}>
            <View style={styles.listCounterContainer}>
              <View style={styles.circlePrimary}>
                <Text style={styles.circleText}>10</Text>
              </View>
            </View>
            <View style={styles.taskView}>
              <Text style={styles.taskText}>
                Malesuada nibh sit amet
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.doneButton}>
                <Text>Done</Text>
              </TouchableOpacity>
            </View>
          </View>

        </ScrollView>

      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 25,
  },
  logoText: {
    fontFamily: 'Roboto',
    fontSize: 30,
    fontWeight: '100',
    paddingHorizontal: 15,
  },
  switchButton:{
    paddingHorizontal:20,
  },
  searchBoxContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    marginTop:20,
    padding: 20,
    justifyContent: 'space-between'
  },
  inputText: {
    flex: 1,
    height: 45,
    borderRadius: 12,
    borderWidth: 0.8,
    borderColor: '#d6d7da'
  },
  roundButtonContainer: {
    marginLeft: 20
  },
  roundButton: {
    height: 45,
    width: 45,
    borderRadius: 45 / 2,
    borderWidth: 1,
    borderColor: '#707070',
    justifyContent: 'center',
    alignItems: 'center'
  },
  roundButtonText:{
    fontSize:25,
  },
  taskList: {
    marginTop: 20
  },
  listItem: {
    padding: 15,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderWidth: 0.7,
    borderTopWidth:0,
    borderColor: '#d6d7da'
  },
  listCounterContainer: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  circlePrimary: {
    height: 45,
    width: 45,
    borderRadius: 45 / 2,
    backgroundColor: '#30C6DE',
    alignItems: "center",
    justifyContent: "center"
  },
  circleText: {
    fontSize: 26,
    color: 'white'
  },
  taskView: {
    flex:1,
    flexDirection: 'row', 
    marginLeft: 10,
    justifyContent:'space-between',
    alignItems:'center'
  },
  taskText: {
    flex:1,
    fontSize: 18
  },
  doneButton:{
    height: 45,
    width: 60,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#707070',
    justifyContent: 'center',
    alignItems: 'center'
  }
});