import React, { Component } from 'react';

import {
    Text,
    View,
    TextInput,
    Button,
    TouchableOpacity,
    Image
  } from 'react-native';

  import styles from './style';
class TaskInputComponent extends Component {
    constructor() {
        super();
        this.state={
            task:''
        }
        this.addItem = this.addItem.bind(this);
    }



    addItem(e) {
        if (this.state.task!= '') {
            this.props.addItem(this.state.task);
            this.setState({
                task:''
            });
        }
        e.preventDefault();
    }
    render() {
        return (

            <View style={styles.searchBoxContainer}>
          <TextInput
            style={styles.inputText}
            underlineColorAndroid='transparent'
            placeholder="Add to My Tasks"
            value={this.state.task}
            onChangeText={(text) => this.setState({task:text})}
          />
          <View style={styles.roundButtonContainer}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={styles.roundButton}
              onPress={this.addItem}
              >
                <Image source={require("../assets/add.png")} tintColor={ "#707070"} style={{width:12, height:12}}/>
                {/* <Text style={styles.roundButtonText}>+</Text> */}
            </TouchableOpacity>
          </View>
        </View>

            // <View style={styles.inputArea}>
            //         <TextInput ref={(a) => this._inputElement = a}
            //             placeholder="Enter task">
            //         </TextInput>
            //         <Button onClick={this.addItem}  title="+">{'+'}</Button>
            // </View>
        );
    }
}

export default TaskInputComponent;
