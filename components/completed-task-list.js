
import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import styles from './style';
import TaskItem from './task-item';
import EmptyTaskList from './empty-task-list';

const CompletedTaskList = (props) => {
  let completedTaskPresent = false;
  return (
    <ScrollView style={styles.taskList}>
      {props.items.map((item, index) => {
        if (item.status == 'completed') {
          completedTaskPresent = true;
          return (<TaskItem item={item} index={index + 1} isUpdate={false} deleteTask={props.deleteTask}/>)
        }
      })
      }

      {
        !completedTaskPresent ? <EmptyTaskList /> : <View />
      }

    </ScrollView>
  )
}

export default CompletedTaskList;

