import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA',
      },
      header: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingVertical: 25,
      },
      logoText: {
        fontFamily: 'Roboto',
        fontSize: 30,
        fontWeight: '100',
        paddingHorizontal: 15,
      },
      switchButton:{
        paddingHorizontal:20,
      },
      searchBoxContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        marginTop:10,
        padding: 15,
        justifyContent: 'space-between'
      },
      inputText: {
        flex: 1,
        height: 45,
        borderRadius: 12,
        borderWidth: 0.8,
        borderColor: '#d6d7da'
      },
      roundButtonContainer: {
        marginLeft: 20
      },
      roundButton: {
        height: 45,
        width: 45,
        borderRadius: 45 / 2,
        borderWidth: 1,
        borderColor: '#707070',
        justifyContent: 'center',
        alignItems: 'center'
      },
      roundButtonText:{
        fontSize:25,
      },
      taskList: {
        marginTop: 20
      },
      listItem: {
        padding: 15,
        flexDirection: 'row',
        backgroundColor: 'white',
        borderWidth: 0.7,
        borderTopWidth:0,
        borderColor: '#d6d7da'
      },
      listCounterContainer: {
        width: 50,
        justifyContent: 'center',
        alignItems: 'center'
      },
      circlePrimary: {
        height: 45,
        width: 45,
        borderRadius: 45 / 2,
        backgroundColor: '#30C6DE',
        alignItems: "center",
        justifyContent: "center"
      },
      circleText: {
        fontSize: 26,
        color: 'white'
      },
      taskView: {
        flex:1,
        flexDirection: 'row', 
        marginLeft: 10,
        justifyContent:'space-between',
        alignItems:'center'
      },
      taskText: {
        flex:1,
        fontSize: 18
      },
      doneButton:{
        height: 45,
        width: 60,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#707070',
        justifyContent: 'center',
        alignItems: 'center'
      }
});

export default styles;