import React, { Component } from 'react';
import {
    AppRegistry,
    Platform,
    StyleSheet,
    Text,
    View,
    ListView,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView,
    AsyncStorage
} from 'react-native';
import styles from './style';
import SwitchSelector from 'react-native-switch-selector';

import TaskInputComponent from './task-input-component';
import HeaderComponent from './header';
import PendingTaskList from './pending-task-list';
import CompletedTaskList from './completed-task-list';
const options = [
    { label: 'To Do', value: '1' },
    { label: 'Completed', value: '2' }
];

export default class ToDoContainer extends Component {

    constructor() {
        super();
        this.state = {
            taskItems: [],
            selectedTab: 1
        }

        this.addItem = this.addItem.bind(this);
        this.doneItem = this.doneItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
    }

    doneItem(index) {
        let items =this.state.taskItems;
        items[index].status = "completed";
        AsyncStorage.setItem('@MySuperStore:to-do-list', JSON.stringify(items));

        this.setState({
            taskItems: items
        });
    }

    deleteItem(index) {
        let items =this.state.taskItems;
        items.splice(index,1);
        AsyncStorage.setItem('@MySuperStore:to-do-list', JSON.stringify(items));

        this.setState({
            taskItems: items
        });
    }


    addItem(task) {
        var newItem = {
            text: task,
            key: Date.now(),
            status: ''
        };

   
       items = this.state.taskItems;
        items.push(newItem);

        items.sort(function (a, b) {
            return new Date(b.key) - new Date(a.key);
        });
        
        AsyncStorage.setItem('@MySuperStore:to-do-list', JSON.stringify(items));

        this.setState({
            taskItems: items
        });
    }

    componentWillMount(){
        AsyncStorage.getItem('@MySuperStore:to-do-list').then((items) => {
            if(items !== null){
                console.log(items);
                this.setState({taskItems : JSON.parse(items)})
            }
        })
      
    }
    render() {
        return (
            <View style={styles.container}>
                <HeaderComponent />
                <View style={styles.switchButton}>
                    <SwitchSelector
                        options={options}
                        initial={0}
                        height={50}
                        buttonColor='#30C6DE'
                        fontSize={15}
                        textColor='#8B8B8B'
                        onPress={value => this.setState({ selectedTab: value })} />
                </View>
                {this.state.selectedTab == 1 ? 
                    <TaskInputComponent addItem={this.addItem} />:<View/>}
                {this.state.selectedTab == 1 ? 
                    <PendingTaskList items={this.state.taskItems} doneTask={this.doneItem} />
                :
                    <CompletedTaskList items={this.state.taskItems} deleteTask={this.deleteItem}/>
                }
            </View>
        );
    }
}
