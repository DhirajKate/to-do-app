
import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import styles from './style';
import TaskItem from './task-item';
import EmptyTaskList from './empty-task-list';

const PendingTaskList = (props) => {
  let pendingTaskPresent = false;
  return (
    <ScrollView style={styles.taskList}>
      {props.items.map((item, index) => {
        if (item.status == '' || item.status == undefined) {
          pendingTaskPresent = true;
          return (<TaskItem item={item} index={index + 1} doneTask={props.doneTask} isUpdate={true}/>)
        }
      })
      }

      {
        !pendingTaskPresent ? <EmptyTaskList /> : <View />
      }

    </ScrollView>
  )
}

export default PendingTaskList;

