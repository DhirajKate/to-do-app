
import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import styles from './style';

const TaskItem = (props) =>(
    <View style={styles.listItem}>
      <View style={styles.taskView}>
        <Text style={styles.taskText}>
          {props.item.text}
        </Text>
        {props.isUpdate?<TouchableOpacity
          activeOpacity={0.6}
          style={styles.doneButton}
          onPress={() => { props.doneTask(props.index-1)}}>
          <Text>Done</Text>
        </TouchableOpacity>:<TouchableOpacity
          activeOpacity={0.6}
          style={styles.doneButton}
          onPress={() => { props.deleteTask(props.index-1)}}>
          <Text>Delete</Text>
        </TouchableOpacity>}
      </View>
    </View>
)
export default TaskItem;

